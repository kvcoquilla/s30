const express = require('express');
const mongoose = require('mongoose');
const port = 4000;
const app = express();

app.use(express.json());
app.use(express.urlencoded({extended:true}));

mongoose.connect('mongodb+srv://admin:admin@prodmongodb.iaelz.mongodb.net/session30?retryWrites=true&w=majority', {


		useNewUrlParser: true,
		useUnifiedTopology: true
})

let db = mongoose.connection;

	db.on("error", console.error.bind(console,"Connection Error"));
	db.once('open', () => console.log('Successfully connected'));

// const taskSchema = new mongoose.Schema({

// 	name: String,
// 	status: {
// 		type: String,
// 		default: 'pending'
// 	}
// });

// const Task = mongoose.model('Task', taskSchema);

// Activity Start

const userSchema =  new mongoose.Schema({

	username: String,
	password: String
});

const Credentials = mongoose.model('Credentials', userSchema);

app.post ('/signup', (req, res) => {

	Credentials.findOne({username: req.body.username}, (err,result) => {


		if(result != null && result.username === req.body.username) {
			return res.send ('Username exist')
		}

		else {
			let newUser = new Credentials ({
				username: req.body.username,
				password: req.body.password
			})

			newUser.save ((saveErr, savedTask) => {

				if(saveErr){
					return console.log(saveErr)
				}

				else {
					return res.status(201).send('New User Created');
				}
			})
		}
	})


})

app.get('/signup', (req, res) => {

	Credentials.find({}, (err, result) => {

		if(err){

			return console.log(err);

		}else {

			return res.status(200).json({

				data: result
			})
		}
	})
})

// Activity End

// app.post('/tasks',(req,res) => {

// 	Task.findOne({name: req.body.name}, (err,result) => {


// 		if(result != null && result.name === req.body.name) {
// 			return res.send ('Duplicate task found.')
// 		}

// 		else {
// 			let newTask = new Task ({
// 				name: req.body.name
// 			})

// 			newTask.save ((saveErr, savedTask) => {

// 				if(saveErr){
// 					return console.log(saveErr)
// 				}

// 				else {
// 					return res.status(201).send('New Task Created');
// 				}
// 			})
// 		}
// 	})


// })

// app.get('/tasks', (req, res) => {

// 	Task.find({}, (err, result) => {

// 		if(err){

// 			return console.log(err);

// 		}else {

// 			return res.status(200).json({

// 				data: result
// 			})
// 		}
// 	})
// })


app.listen(port,() => console.log(`Server is running at port ${port}`));